# Biological Oxygen Monitor datalogger

A simple LabVIEW datalogger for a Jenway 9300 dissolved oxygen sensor.



## Software Requirements

LabVIEW 2021 or later

## License

This project is licenced under the [EUPL Licence](LICENCE.md).

## Contact

For any questions or inquiries, please contact Dirk Geerts (d.j.m.geerts@tudelft.nl).
